<?php
class Board{
	private $theboard=array();
	function __construct($boardsize){
		$GLOBALS['bs']=$boardsize;
		$theboard[0][0]=null;
	}
	public function move($the_str){
		$todo=explode(" ",$the_str,4);
		if(isset($this->theboard[$todo[2]-1][$todo[3]-1])){
                        echo("Данное поле занято\n");
                        return false;
                }
		$figure=namebysymb($this->theboard[$todo[0]-1][$todo[1]-1]);
		//echo $figure;
		$moveok=false;
		$moveok=call_user_func_array(
			"$figure".'::checkmove',
			array(
				array(
					$todo[0],$todo[1]
				),
				array(
					$todo[2],$todo[3]
				)
			)
		);
		if($moveok){
		$this->theboard[$todo[2]-1][$todo[3]-1]=$this->theboard[$todo[0]-1][$todo[1]-1];
		unset($this->theboard[$todo[0]-1][$todo[1]-1]);
		}
		else{
			echo ("Неверный ход $figure\n");
		}
			
	}
	public function insert($the_str){
		$todo=explode(" ",$the_str,3);
		if(isset($this->theboard[$todo[1]-1][$todo[2]-1])){
			echo("Данное поле занято\n");
			return false;
		}
		else
		{if(namebysymb($todo[0])){
                $this->theboard[$todo[1]-1][$todo[2]-1]=$todo[0];
                echo (namebysymb($todo[0])." в координатах (".$todo[1].",".$todo[2].")\n");
                }
		else{
			echo("Данная фигура не определена\n");
			return false;
		}
		}
	}
	public function remove($the_str){
		$todo=explode(" ",$the_str,2);
		if(!isset($this->theboard[$todo[0]-1][$todo[1]-1])){
                        echo("Данное поле свободно\n");
                        return false;
                }
		echo (namebysymb($this->theboard[$todo[0]-1][$todo[1]-1])." в координатах (".$todo[0].",".$todo[1].") удалена с поля\n");
		unset($this->theboard[$todo[0]-1][$todo[1]-1]);
	}
	public function getjson(){
		return json_encode($this->theboard);
	}
	public function setboard($loaded){
		$this->theboard=json_decode($loaded,true);
	}
	public function print(){
		echo("  ");
		foreach (range(1, $GLOBALS['bs']) as $number) {
		     	echo $number." ";
		}
		echo ("\n");
		for($i=0;$i<$GLOBALS['bs'];$i++){
			echo ($i+1)." ";
			for($k=0;$k<$GLOBALS['bs'];$k++){
				echo (isset($this->theboard[$k][$i])?$this->theboard[$k][$i]." ":"0 ");
			};
			echo ("\n");
		}
	}
	

}