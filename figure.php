<?php
function namebysymb($symb){

	$figures=array(
        'b'=>'Bishop',
        'p'=>'Pawn',
        'r'=>'Rook',
);	
	if(array_key_exists($symb,$figures)) {
		return $figures[$symb];
	}
	else {
		return false;
	}
}
abstract class Figure{
	private $symb;
	public abstract static function checkmove($in_pos,$fi_pos);
}
class Bishop extends Figure{
	private $symb="b";
	public static function checkmove($in_pos,$fi_pos){
		if(abs($fi_pos[0]-$in_pos[0])-abs($fi_pos[1]-$in_pos[1])!=0){
                        return false;
		}
                return true;

	}
}
class Rook extends Figure{
	private $symb="r";
	public static function checkmove($in_pos,$fi_pos){
		if(($fi_pos[0]-$in_pos[0])!=0&&($fi_pos[1]-$in_pos[0])!=0){
                        return false;
                }
                return true;
	}
}
class Pawn extends Figure{
	private $symb="p";
	public static function checkmove($in_pos,$fi_pos){
		if(abs($fi_pos[1]-$in_pos[1])==1&&abs($fi_pos[0]-$in_pos[0])==0){
                        return true;
                }
                return false;

	}
}