<?php
interface Storage
{
    public function load();
    public function save($the_data);
}

class FileStorage implements Storage
{
    private $filename="board_state.json";
    public function load()
    {
	    return file_get_contents($this->filename);
    }
    public function save($board_state)
    {
	    file_put_contents($this->filename,$board_state);
    }
}

class RedisStorage implements Storage
{
    private $rds;
    private $host = '127.0.0.1';
    private $port = 6379;
    public function __construct()
    {
	    $this->rds = new Redis();
	    $this->rds->connect($this->host,$this->port);
    }
    public function load()
    {
	    return $this->rds->get('board_state');
    }
    public function save($board_state)
    {
	    $this->rds->set('board_state',$board_state);
    }
}

class Store{
	private $store='File';
	private $sttyp;
	public function __construct(){
		$this->sttyp = new FileStorage();
	}
	public function switchstore($st){
		switch($st)
		{
		case 'f':
			echo ("Хранение в файле board_state.json\n");
			unset($sttyp);
			$this->sttyp=new FileStorage();
			$this->store = 'File';
			break;
		case 'r':
			echo ("Хранение в redis\n");
			$this->store = 'Redis';
			unset($sttyp);
			$this->sttyp=new RedisStorage();
			break;
		default:
			echo("Неверный тип хранилища");
			break;
		}

	}
	public function save($board_state){
		$this->sttyp->save($board_state);
	}
	public function load(){
		return $this->sttyp->load();

	}
}